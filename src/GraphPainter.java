import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.quantego.clp.CLP;
import com.quantego.clp.CLPConstraint;
import com.quantego.clp.CLPExpression;
import com.quantego.clp.CLPVariable;

public class GraphPainter {

	/** number of vertices */
	private static int n;

	/** true if vertex exists, false if not */
	private static boolean[] vertices;

	/** map, index - index of vertex, value - degree of a vertex */
	private static int[] verticesEdges;

	/** adjacency matrix */
	private static boolean[][] adjacencyMatrix;

	/** graph for painting */
	private static Graph cGraph;

	/** set of colors with vertices */
	private static HashMap<Integer, ArrayList<Integer>> colors = new HashMap<Integer, ArrayList<Integer>>();

	/** set of extended colors with vertices */
	private static HashMap<Integer, ArrayList<Integer>> extendedColors = new HashMap<Integer, ArrayList<Integer>>();

	/** pi with star */
	private static double[] piWithStar;
	
	/** maximal independent set */
	private static ArrayList<Integer> maxIndependentSet;
	
	private static int[] getVerticesDegree() {
		int[] degrees = new int[n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (adjacencyMatrix[i][j]) {
					degrees[i]++;
				}
			}
		}
		return degrees;
	}

	private static ArrayList<Integer> getMaxDegreeVertices() {
		int maxDegree = 0;
		ArrayList<Integer> maxDegreeVertices = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			if (verticesEdges[i] >= maxDegree) {
				if (verticesEdges[i] > maxDegree) {
					maxDegree = verticesEdges[i];
					maxDegreeVertices.clear();
				}
				maxDegreeVertices.add(i);
			}
		}
		return maxDegreeVertices;
	}

	public static void paintGraph(Graph graph) {
		//paint with 2 euristics and check which one is better 
		firstEuristic(graph);
		HashMap<Integer, ArrayList<Integer>> colorMap = secondEuristic(graph);
		int firstScore = colors.size();
		int secondScore = colorMap.size();
		if (firstScore > secondScore) {
			colors = colorMap;
		}
		
		extendColors();
		getPrimalSolution();
		System.out.println("Sum pi with star: " + GraphPainter.getSumPiWithStar(graph));
		GraphPainter.runCliquer(graph);
		//if not good enough : traverse tree(); 
	}

	public static void firstEuristic(Graph graph) {
		init(graph);
		while (hasUncoloredVertices()) {
			while (hasEdges()) {
				removeVertices();
			}
			paintVertices();
			reset();
		}
	}

	private static void paintVertices() {
		int colorIndex = colors.size();
		colors.put(colorIndex, new ArrayList<Integer>());
		for (int i = 0; i < n; i++) {
			if (vertices[i]) {
				cGraph.setVertexColor(i, colorIndex);
				colors.get(colorIndex).add(i);
			}
		}
	}

	private static void init(Graph graph) {
		try {
			cGraph = (Graph) graph.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		n = graph.getDimension();

		adjacencyMatrix = graph.getAdjacencyMatrix();

		vertices = new boolean[n];
		for (int i = 0; i < n; i++) {
			vertices[i] = true;
		}

		verticesEdges = getVerticesDegree();
	}

	private static void reset() {
		adjacencyMatrix = cGraph.getAdjacencyMatrix();
		for (int i = 0; i < n; i++) {
			if (cGraph.getVerticesColors()[i] != -1) {
				vertices[i] = false;
				for (int j = 0; j < n; j++) {
					adjacencyMatrix[j][i] = false;
					adjacencyMatrix[i][j] = false;
				}
			} else {
				vertices[i] = true;
			}
		}
		verticesEdges = getVerticesDegree();
	}

	private static boolean hasUncoloredVertices() {
		for (int i = 0; i < n; i++) {
			if (cGraph.getVerticesColors()[i] == -1) {
				return true;
			}
		}
		return false;
	}

	private static void removeVertices() {
		ArrayList<Integer> vertices2remove = getMaxDegreeVertices();
		removeMaxDegreeVertex(vertices2remove);
	}

	private static void removeMaxDegreeVertex(ArrayList<Integer> vertices2remove) {
		int vertexIndex = vertices2remove.get((int) (Math.random() * vertices2remove.size()));
		vertices[vertexIndex] = false;
		for (int i = 0; i < n; i++) {
			adjacencyMatrix[vertexIndex][i] = false;
			adjacencyMatrix[i][vertexIndex] = false;
		}
		verticesEdges = getVerticesDegree();
	}

	private static boolean hasEdges() {
		for (int i = 0; i < n; i++) {
			if (verticesEdges[i] > 0) {
				return true;
			}
		}
		return false;
	}

	public static void print() {
		for (Integer color : colors.keySet()) {
			System.out.print("x" + (color + 1) + " = { ");
			for (Integer vertex : colors.get(color)) {
				System.out.print((vertex + 1) + " ");
			}
			System.out.println("}");
		}
	}
	
	public static void printColors(){
		System.out.println(colors.size());
		for (Integer color : colors.keySet()) {
			for (Integer vertex : colors.get(color)) {
				System.out.print((vertex + 1) + " ");
			}
			System.out.println();
		}
	}

	public static void print(HashMap<Integer, ArrayList<Integer>> coloredVertexes) {
		for (Integer color : coloredVertexes.keySet()) {
			System.out.print("x" + (color + 1) + " = { ");
			for (Integer vertex : coloredVertexes.get(color)) {
				System.out.print((vertex + 1) + " ");
			}
			System.out.println("}");
		}
	}

	public static HashMap<Integer, ArrayList<Integer>> secondEuristic(Graph graph) {
		init(graph);
		boolean[][] matrix = cGraph.getAdjacencyMatrix();
		HashMap<Integer, Integer> weightMap = new HashMap<Integer, Integer>();

		for (int i = 0; i < matrix.length; i++) {
			int weight = 0;
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == true) {
					weight++;
				}
			}
			weightMap.put(i, weight);
		}

		weightMap = sortMapByValues(weightMap);

		boolean isEdge = false;
		int colorNum = 0;
		HashMap<Integer, ArrayList<Integer>> colorMap = new HashMap<Integer, ArrayList<Integer>>();
		ArrayList<Integer> sameColorVertixes = new ArrayList<Integer>();
		ArrayList<Integer> colored = new ArrayList<Integer>();
		for (Integer vertex : weightMap.keySet()) {
			if (!(colored.contains(vertex))) {
				sameColorVertixes.add(vertex);
				colored.add(vertex);
				for (Integer vertex1 : weightMap.keySet()) {
					if (!colored.contains(vertex1)) {
						for (Integer vert : sameColorVertixes) {
							if (matrix[vertex1][vert]) {
								isEdge = true;
								break;
							}
						}
						if (!isEdge) {
							sameColorVertixes.add(vertex1);
							colored.add(vertex1);
						}
						isEdge = false;
					}
				}
				colorMap.put(colorNum, (ArrayList<Integer>) sameColorVertixes.clone());
				sameColorVertixes.clear();
				colorNum++;
			}
		}
		return colorMap;
	}

	@SuppressWarnings("unchecked")
	public static void extendColors() {
		boolean[][] matrix = cGraph.getAdjacencyMatrix();
		ArrayList<Integer> extendedVertixes = new ArrayList<Integer>();
		boolean isEdge = false;
		for (Integer color : colors.keySet()) {
			extendedVertixes = (ArrayList<Integer>) colors.get(color).clone();
			for (int i = 0; i < matrix.length; i++) {
				isEdge = false;
				for (Integer vertex : extendedVertixes) {
					if (matrix[i][vertex] == true) {
						isEdge = true;
						break;
					}
				}
				if (!isEdge) {
					if (!extendedVertixes.contains(i)) {
						extendedVertixes.add(i);
					}
				}
			}
			extendedColors.put(color, new ArrayList<Integer>());
			for (Integer extended : extendedVertixes) {
				extendedColors.get(color).add(extended);
			}
		}
	}

	public static void getPrimalSolution() {
		CLP solver = new CLP();
		ArrayList<CLPVariable> vars = new ArrayList<CLPVariable>(extendedColors.size());
		ArrayList<CLPConstraint> constrs = new ArrayList<CLPConstraint>(n);

		for (int i = 0; i < extendedColors.size(); i++) {
			CLPVariable var = solver.addVariable().lb(0).ub(1);
			vars.add(var);
		}
		
		for (int k = 0; k < n; k++) {
			CLPExpression expr = solver.createExpression();
			for (Integer color : extendedColors.keySet()) {
				for (Integer vertex : extendedColors.get(color)) {
					expr.add(vars.get(color), vertex.equals(k) ? 1 : 0);
				}
			}
			constrs.add(expr.geq(1));
		}
		HashMap<CLPVariable, Double> objFunc = new HashMap<CLPVariable, Double>();
		for (CLPVariable var : vars) {
			objFunc.put(var, 1.0);
		}
		int i = 0;
		
		piWithStar = new double[n];
		for (CLPConstraint constr : constrs) {
			solver.addObjective(objFunc, 0);
			solver.minimize();
			double piStar = solver.getDualSolution(constr);
			piWithStar[i] = piStar;
			System.out.println("i" + (i + 1) + " = "
					+ piStar);
			i++;
		}
	}

	private static HashMap<Integer, Integer> sortMapByValues(Map<Integer, Integer> aMap) {

		Set<Entry<Integer, Integer>> mapEntries = aMap.entrySet();
		List<Entry<Integer, Integer>> aList = new LinkedList<Entry<Integer, Integer>>(mapEntries);
		Collections.sort(aList, new Comparator<Entry<Integer, Integer>>() {

			@Override
			public int compare(Entry<Integer, Integer> ele1, Entry<Integer, Integer> ele2) {

				return ele2.getValue().compareTo(ele1.getValue());
			}
		});
		Map<Integer, Integer> aMap2 = new LinkedHashMap<Integer, Integer>();
		for (Entry<Integer, Integer> entry : aList) {
			aMap2.put(entry.getKey(), entry.getValue());
		}
		return (LinkedHashMap<Integer, Integer>) aMap2;
}

	public static double getSumPiWithStar(Graph graph) {
		findMaxIndependentSet(graph);
		double sumPiWithStar = 0;
		for (Integer vertex : maxIndependentSet) {
			sumPiWithStar += piWithStar[vertex];
		}
		return sumPiWithStar;
	}
	
	private static void findMaxIndependentSet(Graph graph) {
		init(graph);
		maxIndependentSet = new ArrayList<Integer>();
		while (hasVertices()) {
			addVertex();
		}
	}

	private static void addVertex() {
		int vertex = getVertex();
		add2set(vertex);
		removeFromGraph(vertex);
	}

	private static void removeFromGraph(int vertexIndex) {
		vertices[vertexIndex] = false;
		for (int i = 0; i < n; i++) {
			if (adjacencyMatrix[vertexIndex][i]) {
				vertices[i] = false;
			}
			adjacencyMatrix[vertexIndex][i] = false;
			adjacencyMatrix[i][vertexIndex] = false;
		}
		verticesEdges = getVerticesDegree();
	}

	private static void add2set(int vertex) {
		maxIndependentSet.add(vertex);
	}

	private static int getVertex() {
		ArrayList<Integer> vertices = getMinDegreeVertices();
		return vertices.get((int) (Math.random() * vertices.size()));
	}

	private static ArrayList<Integer> getMinDegreeVertices() {
		int minDegree = Integer.MAX_VALUE;
		ArrayList<Integer> minDegreeVertices = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			if (!vertices[i]) {
				continue;
			}
			if (verticesEdges[i] <= minDegree) {
				if (verticesEdges[i] < minDegree) {
					minDegree = verticesEdges[i];
					minDegreeVertices.clear();
				}
				minDegreeVertices.add(i);
			}
		}
		return minDegreeVertices;
	}

	private static boolean hasVertices() {
		for (int i = 0; i < n; i++) {
			if (vertices[i]) {
				return true;
			}
		}
		return false;
	}

	public static void runCliquer(Graph graph) {
		Graph extraGraph = new Graph(getInvertedAdjacencyMatrix(graph));
		int a = 0;
	}

	private static boolean[][] getInvertedAdjacencyMatrix(Graph graph) {
		boolean[][] invertedAdjacencyMatrix = graph.getAdjacencyMatrix();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == j) {
					continue;
				}
				invertedAdjacencyMatrix[i][j] = !invertedAdjacencyMatrix[i][j];
			}
		}
		return invertedAdjacencyMatrix;
	}
}
