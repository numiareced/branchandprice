
public class Graph implements Cloneable {
	
	private int n;
	private int[] verticesColors;
	//index - vertex number, value - color 
	private boolean[][] adjacencyMatrix;
	
	Graph(boolean[][] adjacencyMatrix) {
		
		n = adjacencyMatrix.length;
		this.adjacencyMatrix = adjacencyMatrix;
		
		verticesColors = new int[n];
		for (int i = 0; i < n; i++) {
			verticesColors[i] = -1;
		}
	}
	
	protected Object clone() throws CloneNotSupportedException {
		return new Graph(adjacencyMatrix);
	}
	
	public int getDimension() {
		return n;
	}
	
	public int[] getVerticesColors() {
		return verticesColors;
	}
	
	public void setVertexColor(int vertexIndex, int color) {
		verticesColors[vertexIndex] = color;
	}
	
	public boolean[][] getAdjacencyMatrix() {
		boolean[][] adjacencyMatrix = new boolean[n][n];
		for (int i = 0; i < n; i++) {
			adjacencyMatrix[i] = this.adjacencyMatrix[i].clone();
		}
		return adjacencyMatrix;
	}
}
 