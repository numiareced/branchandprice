
public class Main {
	public static Timer timer;
	public static long time;
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Bad args!!!");
			System.exit(1);
		}
		ColParser colParser = new ColParser(args[0]);
		try {
			time = Long.parseLong(args[1]) * 1000;
		} catch (NumberFormatException e) {
			System.out.println("Wrong time format");
		}
		timer = new Timer();
		timer.start();
		Graph graph = new Graph(colParser.getAdjacencyMatrix());
		GraphPainter.paintGraph(graph);
		GraphPainter.printColors();
	}

}
